const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const talentGroup = require("../controller/talent.group.controller");

router.post("/talentgroup", [authJwt.verifyToken], talentGroup.talentGroup);
router.get(
  "/talentgroup/:talentId",
  [authJwt.verifyToken],
  talentGroup.getTalentGroupById
);
router.get(
  "/getalltalentgroup",
  [authJwt.verifyToken],
  talentGroup.getAllTalentGroup
);
router.put(
  "/talentgroup/:talentId",
  [authJwt.verifyToken],
  talentGroup.updateTalentGroup
);
router.delete(
  "/talentgroup/:talentId",
  [authJwt.verifyToken],
  talentGroup.deleteTalentGroup
);

module.exports = router;
