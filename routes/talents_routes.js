const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const talent = require("../controller/talents.controller");

router.post("/talent", [authJwt.verifyToken], talent.talents);
router.get("/talent/:talentid", [authJwt.verifyToken], talent.getTalentById);
router.get("/talent", [authJwt.verifyToken], talent.getAllTalent);
router.put("/talent/:talentid", [authJwt.verifyToken], talent.updateTalent);
router.delete("/talent/:talentid", [authJwt.verifyToken], talent.deleteTalent);

module.exports = router;
