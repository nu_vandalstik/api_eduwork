const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const coderef = require("../controller/code.ref");

router.post("/coderef", [authJwt.verifyToken], coderef.code_ref);
router.get("/getall", [authJwt.verifyToken], coderef.getAll);
router.get("/coderef/:codeId", [authJwt.verifyToken], coderef.getCodeById);
router.delete(
  "/coderef/:codeId",
  [authJwt.verifyToken],
  coderef.deleteCodeById
);
router.put("/coderef/:codeId", [authJwt.verifyToken], coderef.updateCodeRef);

module.exports = router;
