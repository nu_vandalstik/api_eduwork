const express = require("express");
const router = express.Router();
const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const response = require("../controller/response.controller");

router.post("/response", [authJwt.verifyToken], response.responds);
router.get("/response", [authJwt.verifyToken], response.getAllResponse);
router.get("/response/:resId", [authJwt.verifyToken], response.getResponseById);
router.delete(
  "/response/:resId",
  [authJwt.verifyToken],
  response.deleteResponse
);

module.exports = router;
