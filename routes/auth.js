const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing
const verifySignUp = require("../middlewares/verifySignup");
const authController = require("../controller/auth");
const coderef = require("../controller/code.ref");

router.post(
  "/register",
  [verifySignUp.checkDuplicateUsernameOrEmail, verifySignUp.checkRoleExisted],
  authController.signup
);

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-headers",
    "Authorization, Origin, Content-type, Accept"
  );
  next();
});
router.post("/signin", authController.signin);

// router.post('/coderef', [authJwt.verifyToken, authJwt.isVendor], coderef.code_ref )
module.exports = router;
