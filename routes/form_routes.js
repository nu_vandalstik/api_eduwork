const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing

const form = require("../controller/form.controller");

router.post("/form", [authJwt.verifyToken], form.form);
router.get("/getallform", [authJwt.verifyToken], form.getallform);
router.get("/getform/:formId", [authJwt.verifyToken], form.getformById);
router.delete("/deleteform/:formId", [authJwt.verifyToken], form.deleteform);
router.put("/updateform/:formId", [authJwt.verifyToken], form.updateform);
module.exports = router;
