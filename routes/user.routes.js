const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
const controller = require("../controller/user.controller");

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-headers",
    "Authorization, Origin, Content-type, Accept"
  );
  next();
});

router.get("/test/all", controller.allAccess);

router.get("/test/user", [authJwt.verifyToken], controller.userAccess);
router.get(
  "/test/admin",
  [authJwt.verifyToken, authJwt.isAdmin],
  controller.adminAccess
);

router.get(
  "/test/vendor",
  [authJwt.verifyToken, authJwt.isVendor],
  controller.vendorAccess
);

module.exports = router;
