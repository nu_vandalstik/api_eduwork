const db = require("../models");

const mongoose = require("mongoose");
const Talents = db.talent;
const User = db.user;
const Forms = db.forms;

exports.form = async (req, res) => {
  const form = new Forms({
    title: req.body.title,
    createdBy: req.userId,
    updatedBy: req.userId,
  });

  form.save(async (err, newform) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    if (req.body.talent) {
      Talents.findOne(
        { _id: mongoose.Types.ObjectId(req.body.talent) },
        async (err, talents) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }

          const data = {
            talentid: talents._id,
            reverseFlag: req.body.flag,
            createdBy: req.userId,
            updateBy: req.userId,
          };

          await Forms.findOneAndUpdate(
            { _id: mongoose.Types.ObjectId(newform._id) },
            { $push: { question: data } },
            {
              new: true,
            }
          );

          return res.status(200).send({
            message: "Data Berhasil Di Tambahkan!",
            data: form,
          });

          // form.save(err=>{
          //     if(err){
          //         return res.status(500).send({
          //             message: err
          //         })
          //     }
          //     res.status(200).send({
          //         data: form
          //     })
          // })
        }
      );
    } else {
      res.statu(404).send({
        message: "Data Gagal Di Masukan",
      });
    }
  });
};

exports.getallform = (req, res, next) => {
  Forms.find()
    .then((data) => {
      res.status(200).send({
        message: "Data Form Berhasil DiPanggil",
        data: data,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.getformById = (req, res, next) => {
  const formId = req.params.formId;
  Forms.findById(formId)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Berhasil Ditemukan",
        });
      }

      res.status(200).send({
        message: "Data berhasil Ditemukan",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteform = (req, res, next) => {
  const id = req.params.formId;

  Forms.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Berhasil Ditemukan!",
        });
      }

      return Forms.findByIdAndRemove(id);
    })
    .then((result) => {
      res.status(200).send({
        message: "Delete Form Berhasil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateform = (req, res, next) => {
  const id = req.params.formId;
  Forms.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }

      (result.title = req.body.title),
        (result.talent = req.body.talent),
        (result.question.updateBy = req.userId);

      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Data berhasil DiUpdate!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};
