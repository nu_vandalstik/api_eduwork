const db = require("../models");

const mongoose = require("mongoose");
const Talents = db.talent;
const User = db.user;
const Forms = db.forms;
const Response = db.responds;

exports.responds = async (req, res) => {
  const respons = new Response({
    userId: req.userId,
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    code: req.body.code,
    isDone: req.body.done,
    createdBy: req.userId,
    updateBy: req.userId,
  });

  respons.save(async (err, newresponse) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    if (!req.body.form) {
      return res.status(404).send({
        message: "form harus diisi",
      });
    }
    if (!req.body.talent) {
      return res.status(404).send({
        message: "talent harus diisi",
      });
    }

    await Forms.findOne(
      {
        _id: mongoose.Types.ObjectId(req.body.form),
      },
      async (err, newform) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }

        await Talents.findOne(
          {
            _id: mongoose.Types.ObjectId(req.body.talent),
          },
          async (err, talents) => {
            if (err) {
              return res.status(500).send({
                message: err,
              });
            }

            const result = {
              talentId: talents._id,
              score: req.body.score,
              totalQuestion: req.body.total,
            };

            const respon = {
              talentId: talents._id,
              questionId: newform._id,
              value: req.body.value,
            };

            await Response.findOneAndUpdate(
              {
                _id: mongoose.Types.ObjectId(newresponse._id),
              },
              { $push: { results: result } }
              // {$push: {responds: respon}}
            );

            await Response.findOneAndUpdate(
              {
                _id: mongoose.Types.ObjectId(newresponse._id),
              },
              // {$push: {results: result}}
              { $push: { responds: respon } }
            );

            return res.status(200).send({
              messsage: "Data Berhasil Di Tambahkan",
              Data: respons,
            });
          }
        );
      }
    );
  });
};

exports.getResponseById = (req, res, next) => {
  const id = req.params.resId;
  Response.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Di Temukan",
        });
      }
      res.status(200).send({
        message: "Data Berhasil DiPanggil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteResponse = (req, res, next) => {
  const id = req.params.resId;

  Response.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak DiTemukan!",
        });
      }
      return Response.findByIdAndRemove(id);
    })
    .then((result) => {
      res.status(200).send({
        message: "Delete Response Berhasil!",
        data: result,
      });
    });
};

exports.getAllResponse = (req, res, next) => {
  Response.find()
    .then((result) => {
      res.status(200).send({
        message: "Data Response Berhasil DiPanggil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};
