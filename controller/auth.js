const config = require("../config/auth.config");
const db = require("../models");

const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    phone: req.body.phone,
  });
  user.save((err, user) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    } else if (req.body.roles) {
      Role.find({ name: { $in: req.body.roles } }, (err, roles) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }

        user.roles = roles.map((role) => role._id);
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }
          res.send({
            message: " User Telah Berhasil ditambahkan",
          });
        });
      });
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }
        user.roles = [role._id];
        user.save((err) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }
          res.send({
            message: "user telah berhasil terdaftar",
          });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({
    email: req.body.email,
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        return res.status(500).send({
          message: err,
        });
      }
      if (!user) {
        return res.status(404).send({
          message: "User Tidak Terdaftar !!!",
        });
      }
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(404).send({
          message: "Password Tidak Valid",
        });
      }

      console.log(user.name);
      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400, //24jam
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }

      res.status(200).send({
        id: user._id,
        name: user.name,
        email: user.email,
        roles: user.roles,
        accesToken: token,
      });
    });
};
