const db = require("../models");

const Code = db.code;
const User = db.user;

exports.code_ref = (req, res) => {
  // console.log(req.userId)
  const code = new Code({
    code: req.body.code,
    userId: req.userId,
    maxResponds: req.body.maxResponds,
    createdBy: req.userId,
    updateBy: req.userId,
  });
  code.save((err, code) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }
  });
  res.send({
    id: code._id,
    code: code.code,
    userId: code.userId,
    maxResponds: code.maxResponds,
    timestamps: code.timestamps,
    createdBy: code.userId,
    updateBy: code.userId,
  });
};

exports.getAll = (req, res) => {
  // console.log('masuk')
  Code.find()
    .populate("createdBy")
    .populate("updateBy")
    .exec()
    .then((data) => {
      res.status(200).send({
        data: data,
      });
    })
    .catch((err) => err);
};

exports.getCodeById = (req, res, next) => {
  const codeId = req.params.codeId;
  Code.findById(codeId)
    .populate("createdBy")
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Code Tidak Ditemukan",
        });
      }

      res.status(200).json({
        message: "Data Code Berhasil DiPanggil",
        data: result,
        req: req.userId,
        name: req.name,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteCodeById = (req, res, next) => {
  const codeId = req.params.codeId;

  Code.findById(codeId)
    .then((post) => {
      if (!post) {
        res.status(404).send({
          message: "Data Code Tidak Ditemukan",
        });
      }
      return Code.findByIdAndRemove(codeId);
    })
    .then((result) => {
      res.status(200).json({
        message: "Data Code Berhasil di Hapus",
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateCodeRef = (req, res, next) => {
  const codeId = req.params.codeId;

  Code.findById(codeId)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }
      (result.code = req.body.code),
        (result.maxResponds = req.body.maxResponds);

      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Update Data Code Ref Berhasil!",
        data: result,
      });
    });
};
