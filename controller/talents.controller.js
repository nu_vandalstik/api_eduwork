const db = require("../models");

const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);
const Talents = db.talent;
const talentsGroup = db.talentGroup;
//sip nah sekarang ganti yang groupId kan masih array tu

exports.talents = async (req, res) => {
  const tg = await talentsGroup.findOne({
    _id: mongoose.Types.ObjectId(req.body.groupId),
  });
  console.log(req.body.groupId);
  console.log(tg);
  const talents = new Talents({
    name: req.body.name,
    description: req.body.description,
    priorOrder: req.body.priorOrder,
    createdBy: req.userId,
    updatedBy: req.userId,
    groupId : req.body.groupId
  
  });


  talents.save( async(err, newtalent) => {
    // console.log(talents)
    if (err) {
      return res.status(500).send({
        message: err,
      });
    } else if (req.body.groupId) {
      //Nah ini search aja by _id, karena nanti name cuma diliatin di frontend
      //pas ngelempar dia ngelmpar groupId
      //import mongoosenya dulu mas


      await talentsGroup.updateOne({ _id: mongoose.Types.ObjectId(req.body.groupId) }, { $push: { talents: mongoose.Types.ObjectId(newtalent._id) } })
      // newtalent.groupId = talentGroup._id;
      
          // newtalent.save((err) => {
          //   if (err) {
          //     return res.status(500).send({
          //       message: err,
          //     });
          //   }
            
          // });

          res.status(200).send({
            data: newtalent,
          });

      // talentsGroup.updateOne(
      //   { _id: mongoose.Types.ObjectId(req.body.groupId) },
      //   (err, talentGroup) => {
      //     // console.log(talentGroup);
      //     if (err) {
      //       return res.status(500).send({
      //         message: err,
      //       });
      //     }

      //     //bisa pakai talentGroup._id atau req.body.groupId, bebas soalnya valuenya sama
      //     // talents.groupId = groupId nya itu adalah nama key nya
    
      //   }
      // );
    } else {
      res.status(404).send({
        message: "Data Tidak Tersedia",
      });
    }
  });
};

exports.getTalentById = (req, res, next) => {
  const id = req.params.talentid;

  Talents.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Talent Tidak DiTemukan!",
        });
      }

      res.status(200).send({
        message: "Data Talent Berhasil DiTemukan!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.getAllTalent = (req, res, next) => {
  Talents.find()
    .then((result) => {
      res.status(200).send({
        message: "Data Talent Berhasil DiPanggil!",
        message: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateTalent = (req, res, next) => {
  const id = req.params.talentid;
  Talents.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Talent Tidak DiTemukan!",
        });
      }

      (result.name = req.body.name),
        (result.description = req.body.description),
        (result.priorOrder = req.body.priorOrder);

      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Update Data Talent Berhasil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteTalent = async (req, res, next) => {
  const id = req.params.talentid;

 


  Talents.findById(id)
    .then(async(result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }
      await talentsGroup.updateMany(
        { },
        { $pull: { talents: mongoose.Types.ObjectId(id) } }
      )

      return Talents.findByIdAndRemove(id);
    })
    .then((result) => {
      res.status(200).send({
        message: "Data Talent Berhasil DiHapus!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

// 60f3feda6335651bf42fc7e8