const db = require("../models");

const mongoose = require("mongoose");
const talentsGroup = db.talentGroup;
const talents = db.talent;
const User = db.user
;
exports.talentGroup = async (req, res) => {
  const tg = await talents.findOne({
    _id: mongoose.Types.ObjectId(req.body.idtalents),
  });
  // console.log(req.body.idtalents)
  // console.log(tg)

  const talentGroup = new talentsGroup({
    name: req.body.name,
    description: req.body.description,
    brainSide: req.body.brainside,
    createdBy: req.userId,
    updateBy: req.userId,
    // idtalents
  });
  talentGroup.save((err, talentGroups) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    // console.log(req.body.idtalents)
    else if (req.body.idtalents) {
      // console.log('masuuuk')
      talents.findOne(
        { _id: mongoose.Types.ObjectId(req.body.idtalents) },
        (err, talents) => {
          // console.log(talents);
          // console.log(err);
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }

          talentGroups.talents = talents._id;

          talentGroup.save((err) => {
            console.log("masuk");

            if (err) {
              return res.status(500).send({
                message: err,
              });
            }

            res.status(200).send({
              data: talentGroup,
            });
          });
        }
      );
    } else {
      res.status(404).send({
        message: "Data Tidak Tersedia",
      });
    }
  });
};

exports.getAllTalentGroup = (req, res, next) => {
  talentsGroup
    .find()
    .populate("talents").exec()
    .then((result) => {
      res.status(200).send({
        message: "Data Talent Group Berhasil Di Panggil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.getTalentGroupById = (req, res, next) => {
  id = req.params.talentId;

  talentsGroup
    .findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Talent Group Tidak DiTemukan",
        });
      }

      res.status(200).send({
        message: "Data Talent Group DiTemukan",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateTalentGroup = (req, res, next) => {
  id = req.params.talentId;
  talentsGroup
    .findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Talent Group Tidak Ditemukan!",
        });
      }

      (result.name = req.body.name),
        (result.description = req.body.description),
        (result.brainSide = req.body.brainside),
        (result.updateBy = req.userId);

      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Update Talent Group Berhasil",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteTalentGroup = (req, res, next) => {
  id = req.params.talentId;

  talentsGroup
    .findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }
      return talentsGroup.findByIdAndRemove(id);
    })
    .then((result) => {
      res.status(200).send({
        message: "Talent Group Berhasil DiHapus!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};
