const mongoose = require("mongoose");

let child = new mongoose.Schema(
  {
    talentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "talents",
    },
    reverseFlag: {
      type: Boolean,
      default: false,
    },

    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    updateBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
  },
  {
    timestamps: true,
  }
);

const Forms = mongoose.model(
  "forms",
  new mongoose.Schema(
    {
      title: String,
      question: [child],
      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
      },
      updateBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
      },
    },
    {
      timestamps: true,
    }
  ),
  "forms"
);

module.exports = Forms;
