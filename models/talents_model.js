const mongoose = require("mongoose");

const Talents = mongoose.model(
  "talents",
  new mongoose.Schema(
    {
      name: String,
      description: String,
      priorOrder: Number,
      groupId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "talentGroups",
      },
      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },

      updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    },
    {
      timestamps: true,
    }
  ),
  "talents"
);

module.exports = Talents;
