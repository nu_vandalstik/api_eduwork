const mongoose = require("mongoose");
const { Schema } = mongoose;

const talentGroups = mongoose.model(
  "talentGroups",
  new mongoose.Schema(
    {
      name: String,
      description: String,
      brainSide: String,
      talents: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "talents",
        },
      ],

      createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
      },
      updateBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
      },
    },
    {
      timestamps: true,
    }
  ),
  "talentGroups"
);

module.exports = talentGroups;
