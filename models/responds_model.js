const mongoose = require("mongoose");

let child = new mongoose.Schema({
  talentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "talents",
  },
  score: Number,
  totalQuestion: Number,
});

let newchild = new mongoose.Schema({
  talentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "talents",
  },
  questionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "forms",
  },
  value: Number,
});

const responds = mongoose.model(
  "responds",
  new mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    name: String,
    email: String,
    phone: String,
    code: String,
    isDone: Boolean,
    results: [child],
    responds: [newchild],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    updateBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
  }),
  "responds"
);

module.exports = responds;
