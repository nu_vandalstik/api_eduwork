const mongoose = require("mongoose");

// import mongoose from 'mongoose';
const { Schema } = mongoose;

const codeReferenceSchema = new Schema(
  {
    code: String,
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    maxResponds: Number,
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    updateBy: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

// const monggose = require('mongoose')

// const schema = new monggose.Schema({
//     code: String,
//     userId: {
//         type: Schema.Types.ObjectId,
//         ref: "User"
//     },
//     maxResponds: Number,
//     createdBy: {
//         type: Schema.Types.ObjectId,
//         ref: "User"
//     },
//     updatedBy: {
//         type: Schema.Types.ObjectId,
//         ref: "User"
//     },

// },
//     {
//         timestamps:true
//     }
// )

// const CodeReferences = mongoose.model(
//     "CodeReferences", schema, "code_references"

// )

const codeReference = mongoose.model("CodeReference", codeReferenceSchema);
module.exports = codeReference;
